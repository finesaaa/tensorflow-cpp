# TensorFlow C++ API with OpenCV4
To install Tensorflow C++ API, **Bazel, Protobuf, and Eigen** are needed.

## (Optional) Install **CUDA and cuDNN driver**
 
## Install **Bazel**

For **TF-branch (r1.8)**, **Bazel 0.10.0** works fine. Check bazel version for other version of tensorflow [more](https://www.tensorflow.org/install/source).
- Install required packages
``` sh 
~$ sudo apt install g++ unzip zip
# Ubuntu 18.04 (LTS) uses OpenJDK 11 by default:
~$ sudo apt install openjdk-11-jdk
```
- Run the installer
``` sh
~$ chmod +x bazel-<version>-installer-linux-x86_64.sh
~$ ./bazel-<version>-installer-linux-x86_64.sh --user
```
- Set up the environtment
``` sh
~$ export PATH="$PATH:$HOME/bin"
```

## Install Protobuf
For we need C++ version, we need to download the source package and compile it (NOT use the linux_x86_64 version). Protobuf 3.5.1 works fine.
``` sh
~$ sudo apt-get install autoconf automake libtool curl make
~$ curl -OL https://github.com/protocolbuffers/protobuf/releases/download/v3.5.0/protobuf-cpp-3.5.0.zip
~$ unzip protoc-3.5.1.zip && cd protoc-3.5.1
~$ ./autogen.sh
~$ ./configure
~$ make && sudo make install
~$ sudo ldconfig
```

## Install Eigen
Eigen 3.3.4 works fine. PS: Don’t use libeigen3-dev via apt-get install. This package may be obsolete which cause compiling error.
``` sh
# Download eigen 3.3.4 from https://github.com/eigenteam/eigen-git-mirror/releases
~$ unzip eigen-git-mirror-3.3.4.zip
~$ cd eigen-git-mirror
~$ mkdir build && cd build
~$ cmake ..
~$ make && sudo make install
```

## Install Tensorflow
Here use **Tensorflow r1.8 version**
- Install Python and the TensorFlow package dependencies
``` sh
~$ sudo apt-get install python3-numpy python3-dev python3-pip python3-wheel
~$ pip install -U --user pip six numpy wheel setuptools mock 
~$ pip install -U --user keras_applications --no-deps
~$ pip install -U --user keras_preprocessing --no-deps
```
- Download the TensorFlow source code
``` sh
~$ git clone https://github.com/tensorflow/tensorflow.git
~$ cd tensorflow
~$ git checkout <branch_name> # r1.8, r1.9, etc
```
- Configure the build [more](https://www.tensorflow.org/install/source)
``` sh
./configure
# configure build option, we can set to build with CUDA support or not etc
```

- Build the pip package and the package
``` sh
# GPU support
~$ bazel build --config=opt --config=cuda //tensorflow/tools/pip_package:build_pip_package
# CPU-only
~$ bazel build --config=opt //tensorflow/tools/pip_package:build_pip_package
~$ bazel-bin/tensorflow/tools/pip_package/build_pip_package /tmp/tensorflow_pkg
```

- Install the package 
``` sh
~$ sudo pip install /tmp/tensorflow_pkg/tensorflow-1.8.0-py3-none-any.whl
```

**Success: TensorFlow is now installed.**

- Install Tensorflow C++ API 
Attention here: if you want to use C API, buildtensorflow/libtensorflow.so, if C++ API, use tensorflow/libtensorflow_cc.so.
``` sh
~$ bazel build --cxxopt=-std=c++11 --config=monolithic //tensorflow:libtensorflow_cc.so

 # copy required files into a single path for c++ linkage
~$ sudo mkdir /usr/local/include/tf  
~$ sudo mkdir /usr/local/include/tf/tensorflow
~$ sudo cp -r bazel-genfiles/ /usr/local/include/tf
~$ sudo cp -r tensorflow/cc /usr/local/include/tf/tensorflow
~$ sudo cp -r tensorflow/core /usr/local/include/tf/tensorflow
~$ sudo cp -r third_party /usr/local/include/tf
~$ sudo cp bazel-bin/tensorflow/libtensorflow_cc.so /usr/local/lib
~$ sudo cp bazel-bin/tensorflow/libtensorflow_framework.so /usr/local/lib
 ```
**Success: TensorFlow C++ API is now installed.**

- (Optional) Install/Update Cmake
System default Cmake version may be too low to compile C++ project successfully. To update cmake , you should choose and download from from this [site](https://cmake.org/download/). Cmake 3.11.1 works fine for me.
``` sh
~$ tar xcvf cmake-3.11.1-Linux-x86_64.tar.gz
~$ cd cmake-3.11.1-Linux-x86_64
~$ sudo apt-get purge cmake
~$ sudo cp -r bin /usr/
~$ sudo cp -r share /usr/
~$ sudo cp -r doc /usr/share/
~$ sudo cp -r man /usr/share/
```

## Install OpenCV 4.2.0
Tutorial in [GitLab/ICHIRO-ITS/Documentation](https://gitlab.com/ICHIRO-ITS/documentation/-/blob/master/Setup/OpenCV-Setup.md)

## Test with Sample Project
``` sh
~$ mkdir build && cd build cmake ..
~$ sudo make
~$ ./tf_detector_example
```
Usage:
1. Specify your own paths for necessary libs in `CmakeLists.txt`
2. Specify your own paths for `frozen_inference_graph_person.pb` and `Person.pbtxt` in `main.cpp` (lines 44-47)
3. Specify your video source (`main.cpp`, line 80)

PS: Don't forget to link OpenCV4